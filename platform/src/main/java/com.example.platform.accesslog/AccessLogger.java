package com.example.platform.accesslog;

public interface AccessLogger {
    String name();

    void log(String var1);
}

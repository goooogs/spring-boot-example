package com.example.platform.accesslog.springboot;

import com.example.platform.accesslog.AccessLoggerInstanceHolder;
import com.example.platform.accesslog.Slf4jAccessLogger;
import com.example.platform.accesslog.jetty.JettyAccessRequestLog;
import org.springframework.boot.web.embedded.jetty.JettyServerCustomizer;
import org.springframework.boot.web.embedded.jetty.JettyServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;

public class EmbeddedJettyCustomizer implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> {
    private final boolean logResponseSize;

    public EmbeddedJettyCustomizer(boolean logResponseSize) {
        this.logResponseSize = logResponseSize;
        AccessLoggerInstanceHolder.updateAccessLogger(new Slf4jAccessLogger());
    }

    public void customize(ConfigurableServletWebServerFactory container) {
        JettyServletWebServerFactory c = (JettyServletWebServerFactory)container;
        c.addServerCustomizers(new JettyServerCustomizer[]{(server) -> {
            JettyAccessRequestLog requestLog = new JettyAccessRequestLog();
            requestLog.setLogResponseSize(this.logResponseSize);
            server.setRequestLog(requestLog);
        }});
    }
}

package com.example.platform.common.exception;

public class AuthenticationException extends ServiceException {
    public AuthenticationException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }

    public AuthenticationException(String errCode, String errMsg, Throwable cause) {
        super(errCode, errMsg, cause);
    }
}
package com.example.platform.common.exception;

public class ResourceNotFoundException extends ServiceException {
    public ResourceNotFoundException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }

    public ResourceNotFoundException(String errCode, String errMsg, Throwable cause) {
        super(errCode, errMsg, cause);
    }
}
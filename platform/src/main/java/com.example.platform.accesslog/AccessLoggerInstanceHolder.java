package com.example.platform.accesslog;

public final class AccessLoggerInstanceHolder {
    private static volatile AccessLogger instance = new NopAccessLogger();

    public AccessLoggerInstanceHolder() {
    }

    public static void updateAccessLogger(AccessLogger accessLogger) {
        Class var1 = AccessLoggerInstanceHolder.class;
        synchronized(AccessLoggerInstanceHolder.class) {
            instance = accessLogger;
            System.out.println(String.format("Bind access logger with %s", accessLogger.getClass().getName()));
        }
    }

    public static AccessLogger getSingletion() {
        return instance;
    }
}
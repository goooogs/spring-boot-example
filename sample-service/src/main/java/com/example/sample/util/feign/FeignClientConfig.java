package com.example.sample.util.feign;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import feign.Client;
import feign.Contract;
import feign.Logger;
import feign.RequestInterceptor;
import feign.Retryer;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.codec.ErrorDecoder;

@Data
public class FeignClientConfig {

  /**
   * {@code null} means it hasn't been overridden
   */
  private Client client; // default is ApacheHttpClient
  private Logger logger; // default is Slf4jLogger
  private Logger.Level loggerLevel; // default is Slf4jLogger
  private Contract contract; // default is Contract.Default()
  private Encoder encoder; // default is JacksonEncoder
  private Decoder decoder; // default is JacksonDecoder
  private ErrorDecoder errorDecoder; // default is FeignClientErrorDecoder
  private final List<RequestInterceptor> requestInterceptors = new ArrayList<>();
  private Retryer retryer; // default is Retryer.NEVER_RETRY

  public FeignClientConfig addInterceptor(RequestInterceptor interceptor) {
    this.requestInterceptors.add(interceptor);
    return this;
  }

}

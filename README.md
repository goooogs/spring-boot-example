spring-boot-example
=======================
这是一个基于 Spring Boot 实现的 Hello world 项目。

# 项目说明
- platform 中封装了一些异常，以及日志标准化（拆分出 Spring Boot 日志和 API 访问日志）
- sample-service 包含 API 接口的实现
- sample-spi 是对外提供的 SPI

# 编译环境
- gradle: 5.2.1
- jdk: 8

# 运行环境
- 系统变量
    - `-DAppLogs=/log/output/directory` 指定标准化日志输出目录
- 环境变量
    - JAVA_OPTS 配置位于 ConfigMap
- APM
    - Elastic APM Agent 位于基础镜像内，通过 ConfigMap 中配置的 JAVA_OPTS 来控制是否加载
- jdk: 8

# Demo 展示
- 项目运行示例: [Hello world](http://ab02064b318c64a199b203b3e60be6cc-534599681.cn-northwest-1.elb.amazonaws.com.cn:10010/swagger-ui.html)

    图一：项目运行示例
    ![01-项目运行示例](docs/images/01-app-swagger-ui.png)

- APM 展示地址: [Elastic APM](https://6a506a2126e4461995e0cb41f6889d2f.ap-northeast-1.aws.found.io:9243/app/apm) 

    图二：APM 示例-Transactions
    ![02-APM示例-事物](docs/images/02-apm-transactions.png)

    图三：APM 示例-JVM
    ![03-APM示例-JVM](docs/images/03-apm-jvm.png)

    图四：APM 示例-ServiceMap
    ![04-APM示例-ServiceMap](docs/images/04-apm-service-map.png)

- 日志: [Elastic Kibana](https://6a506a2126e4461995e0cb41f6889d2f.ap-northeast-1.aws.found.io:9243/app/home#/)

    图五：API 访问日志
    ![01-项目运行示例](docs/images/05-kibana-api-access-log.png)

    图六：Spring Boot 日志
    ![02-APM示例-事物](docs/images/06-kibana-app-log.png)

# 编译和更新
- 新的提交会自动触发 [Jenkins](http://a5b4113560dce4655a703e41f574db58-1903955992.cn-northwest-1.elb.amazonaws.com.cn:10000/) 完成构建和发布
- Artifactory 存放在 [JFrog](http://52.82.40.171:8082/)
    - snapshot repo: [repo15657581394-maven-snapshot-local](http://52.82.40.171:8082/ui/repos/tree/General/repo15657581394-maven-snapshot-local)
    - release repo: [repo15657581394-maven-release-local](http://52.82.40.171:8082/ui/repos/tree/General/repo15657581394-maven-release-local)
    - build info: [spring-boot-example](http://52.82.40.171:8082/ui/builds/spring-boot-example/194/1602921936707/published)
- Jenkins Pipeline repo: [jenkins-pipeline](https://gitlab.com/goooogs/jenkins-pipeline.git)

package com.example.sample.util.feign;

import lombok.Getter;

import org.springframework.util.StringUtils;

@Getter
public class RemoteAppConfig {

  private String remoteAppId;
  private String url;
  // 服务级别连接池配置
  private int connectTimeout = 2000;
  private int readTimeout = 5000;
  private int maxTotalConnections = 20;
  private int maxPerRouteConnections = 10;

  private RemoteAppConfig(String remoteAppId, String url, int connectTimeout, int readTimeout,
                          int maxTotalConnections, int maxPerRouteConnections) {
    if (StringUtils.isEmpty(remoteAppId) || StringUtils.isEmpty(url)) {
      throw new IllegalArgumentException("Remote appId and url must not null");
    }
    this.remoteAppId = remoteAppId;
    this.url = url;
    if (connectTimeout > 0) {
      this.connectTimeout = connectTimeout;
    }
    if (readTimeout > 0) {
      this.readTimeout = readTimeout;
    }
    if (maxTotalConnections > 0) {
      this.maxTotalConnections = maxTotalConnections;
    }
    if (maxPerRouteConnections > 0) {
      this.maxPerRouteConnections = maxPerRouteConnections;
    }
  }

  public static RemoteAppConfigBuilder newBuilder() {
    return new RemoteAppConfigBuilder();
  }

  public static class RemoteAppConfigBuilder {

    private String remoteAppId;
    private String url;
    private int connectTimeout;
    private int readTimeout;
    private int maxTotalConnections;
    private int maxPerRouteConnections;

    public RemoteAppConfigBuilder remoteAppId(String remoteAppId) {
      this.remoteAppId = remoteAppId;
      return this;
    }

    public RemoteAppConfigBuilder url(String url) {
      this.url = url;
      return this;
    }

    public RemoteAppConfigBuilder connectTimeout(int connectTimeout) {
      this.connectTimeout = connectTimeout;
      return this;
    }

    public RemoteAppConfigBuilder readTimeout(int readTimeout) {
      this.readTimeout = readTimeout;
      return this;
    }

    public RemoteAppConfigBuilder maxTotalConnections(int maxTotalConnections) {
      this.maxTotalConnections = maxTotalConnections;
      return this;
    }

    public RemoteAppConfigBuilder maxPerRouteConnections(int maxPerRouteConnections) {
      this.maxPerRouteConnections = maxPerRouteConnections;
      return this;
    }

    public RemoteAppConfig build() {
      return new RemoteAppConfig(this.remoteAppId, this.url, this.connectTimeout, this
          .readTimeout, this.maxTotalConnections, this.maxPerRouteConnections);
    }

  }


}

package com.example.sample.config;

import com.example.sample.client.SampleClient;
import com.example.sample.util.feign.FeignClientBuilder;
import com.example.sample.util.feign.FeignClientConfig;
import com.example.sample.util.feign.RemoteAppConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * TODO REPLACE_ME 替换remoteAppId, url等信息,这里示例是sample调用sample自己
 */
@Configuration
public class FeignClientConfiguration {

  @Value("${client.sample.url:http://127.0.0.1:8080}")
  private String sampleUrl;

  @Autowired
  private FeignClientBuilder feignClientBuilder;

  @Bean(destroyMethod = "close")
  public FeignClientBuilder feignClientBuilder() {
    return new FeignClientBuilder("sample");
  }

  @Bean("sampleRemoteConfig")
  public RemoteAppConfig sampleRemoteConfig() {
    return RemoteAppConfig.newBuilder().remoteAppId("sample").url(sampleUrl)
        // 可选配置,可根据具体情况设置
        .connectTimeout(2000).readTimeout(5000)
        .maxTotalConnections(50).maxPerRouteConnections(20)
        .build();
  }

  @Bean
  public SampleClient sampleClient(@Qualifier("sampleRemoteConfig") RemoteAppConfig
                                       sampleRemoteConfig) {
    FeignClientConfig clientConfig = new FeignClientConfig()
        .addInterceptor(template -> template.header("my-header", "my-value"));
    // clientConfig设置{@code null}使用默认配置, 配置后即覆盖默认配置
    return feignClientBuilder.buildClient(SampleClient.class, sampleRemoteConfig, clientConfig);
  }
}

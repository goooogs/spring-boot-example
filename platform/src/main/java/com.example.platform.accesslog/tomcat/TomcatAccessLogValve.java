package com.example.platform.accesslog.tomcat;

import com.example.platform.accesslog.internal.AccessLogUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import org.apache.catalina.AccessLog;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

public class TomcatAccessLogValve extends ValveBase implements AccessLog {
    private boolean requestAttributesEnabled = false;
    private boolean logResponseSize = true;

    public TomcatAccessLogValve() {
        super(true);
    }

    public void log(Request request, Response response, long time) {
        if (this.getState().isAvailable()) {
            AccessLogUtil.log(request, response, time, this.logResponseSize ? response.getBytesWritten(false) : -1L);
        }
    }

    public void setRequestAttributesEnabled(boolean requestAttributesEnabled) {
        this.requestAttributesEnabled = requestAttributesEnabled;
    }

    public boolean getRequestAttributesEnabled() {
        return this.requestAttributesEnabled;
    }

    public void invoke(Request request, Response response) throws IOException, ServletException {
        this.getNext().invoke(request, response);
    }

    public void setLogResponseSize(boolean logResponseSize) {
        this.logResponseSize = logResponseSize;
    }
}

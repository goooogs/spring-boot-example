package com.example.platform.accesslog;

public class NopAccessLogger implements AccessLogger {
    public NopAccessLogger() {
    }

    public String name() {
        return "nop";
    }

    public void log(String info) {
    }
}
package com.example.platform.common.exception.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtil {
    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
    private static ObjectMapper objectMapper;

    public JsonUtil() {
    }

    public static <T> T jsonToObject(String json, Class<T> toValueType) {
        if (json == null) {
            return null;
        } else {
            try {
                return objectMapper.readValue(json, toValueType);
            } catch (Exception var3) {
                log.error("JSON to object, JSON: {}, object class: {}", new Object[]{json, toValueType.getName(), var3});
                throw new IllegalArgumentException("JSON to object", var3);
            }
        }
    }

    static {
        objectMapper = (new ObjectMapper()).setSerializationInclusion(Include.NON_NULL).configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }
}
